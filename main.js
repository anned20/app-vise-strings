const assert = require('assert');

// Cases to test and validate against
const testCases = [
    {
        string1: 'xyaabbbccccdefww',
        string2: 'xxxxyyyyabklmopq',
        validation: 'abcdefklmopqwxy',
        equal: true,
    }, {
        string1: 'xyaabbbccccdefww',
        string2: 'xxxxyyyyabklmopq',
        validation: 'abcdefklmopqwxyz',
        equal: false,
    }, {
        string1: 'abcdefghijklmnopqrstuvwxyz',
        string2: 'abcdefghijklmnopqrstuvwxyz',
        validation: 'abcdefghijklmnopqrstuvwxyz',
        equal: true,
    },
];

// Converts string to set to filter out duplicates and converts to array
const getUniqueChars = (string) => [...new Set(string)];

testCases.map((testCase) => {
    const uniqueChars = getUniqueChars(testCase.string1 + testCase.string2);

    const toValidate = uniqueChars.sort().join('');

    if (testCase.equal) {
        assert.equal(toValidate, testCase.validation);
    }

    if (!testCase.equal) {
        assert.notEqual(toValidate, testCase.validation);
    }
});

console.log('Assertions passed');
